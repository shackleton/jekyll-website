---
layout: post
title: Principios Fundamentales Barco TecnoPiratas
---

![images](/assets/tecnopiratas2.png)

### Tecno Piratas es un barco temático auto-organizado, autónomo e independiente de los partidos políticos piratas.  Ayuda en la formación e investigación del impacto sociocultural, político, legal  y económico de la informática y la internet en la sociedad. Promueve los derechos digitales, la libre expresión, la ética hacker y la cultura del software libre. Constituido principalmente para contribuir a la determinación de la política nacional en temas de tecnologías y transformación digital en el estado chileno, como también la formación de la voluntad política en los ciudadanos bajo una cultura tecnológica emancipada y responsable de la informática individual.

