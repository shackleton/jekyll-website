---
layout: page
title: Autodefensa Digital
---
![Image](/assets/e-democracy.jpg)

Si alguna vez te preocupó proteger la privacidad de tus datos digitales o tus conversaciones con otras personas, estamos aquí para ayudarte.

Autoprotección Digital Contra La Vigilancia es una guía de seguridad digital que te enseña a evaluar tu riesgo personal del espionaje en línea. Puede ayudarte a protegerte de la vigilancia de aquellos que quieran descubrir tus secretos, desde pequeños delincuentes hasta estados nacionales. Ofrecemos guías sobre las mejores herramientas para mejorar la privacidad y explicamos cómo incorporar protección contra la vigilancia en tu rutina diaria.

Si estás listo para dar los primeros pasos, nuestra serie de guías básicas (a continuación) te ayudarán a comprender qué es la vigilancia digital y cómo puedes combatirla. Sugerimos comenzar con la guía Evaluando tus riesgos.

[Colectivo Disonancia - Autodefensa Digital Protesta](/assets/AutodefensaDigital.pdf)

¡Ayuda a que la vigilancia masiva de poblaciones enteras no sea económica! 
Todos tenemos derecho a la privacidad, que puede ejercer hoy encriptando sus comunicaciones y poniendo fin a su dependencia de los servicios patentados.

[https://prism-break.org/](https://prism-break.org/en/all/)
