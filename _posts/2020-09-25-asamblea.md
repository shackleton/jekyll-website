---
layout: post
title: Asamblea Viernes 25 de Septiembre
author: Shackleton
---

Acta 003
TecnoPiratas
25 de Septiembre
22.30 hrs


ASISTENTES:

    @Shackleton
    @Pato_Acevedo
    @Alejandro_mellao
    @David_ormeno
    @nicolas
    @felix
    @jindelpiojoso

Tabla:

    Cursos de Tecnica & Cultura Hacker.
        Programacion en Ruby on Rails. (12 personas minimo)
            FECHAS DE INICIO
                Octubre (Lunes y Jueves 19.00 hrs)
            FECHAS DE FINALIZACION
                Diciembre (Semana de Navidad)
                (Taller orientado a suplementar DECIDIM)
                Programa de estudios.
                    introduccion
                    naturaleza
                    sintaxis
                    framework Rails
                    Marco Teorico.
                    Valores de Participacion.
                        $2000 Aporte minimo por clase.

    Estado DECIDIM
        Servidor de desarrollo (Demostracion)
            https://shishigang.demo.participa.cloud/
            aLabs Tenemos acceso SSH, para copiar el proyecto.
            Grupo de Telegram con la gente de Decidim Espana, aLab.
                Reunion, para concretar alcances del sistema.
                    OBJETIVO: COPIAR EL PROYECTO DESDE EL SERVIDOR shishigang.demo.participa.cloud
        Servidores de Produccion
            https://decidim.partidopirata.cl
            https://cabildosdigitales.cl
            LINODE

    Repositorios Estatales:
        https://git.gob.cl/software-publico/decidim-chile-que-queremos
