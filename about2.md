---
layout: page
title: Contra la Vigilancia
---

Algunos Gobiernos espían todo lo que hacemos en Internet. Los documentos que [Edward Snowden](https://en.wikipedia.org/wiki/Edward_Snowden) hizo públicos en 2013, por lo cual tuvo que huir de Estados Unidos, han revelado cómo las agencias de seguridad estatales utilizan la vigilancia masiva para recoger, almacenar y analizar en secreto millones de comunicaciones privadas de personas en todo el mundo. Cuando los Gobiernos nos espían de esta manera están violando nuestros derechos humanos.

![images](/assets/vigilancia.jpg)

El dominio de Internet por los poderes estatales o comerciales, o, muy a
menudo, una asociación de los dos, parece total, y lo es efectivamente
donde los vectores y las plataformas son 'propietarios', es decir cuando están
en posesión de actores particulares quienes pondrán por delante sus
intereses propios, con frecuencia a costa de los intereses de sus usuari@s.
Mientras que el impacto que tiene Internet en nuestras vidas se hace cada
vez más fuerte, una toma de conciencia acerca de cómo, y sobre todo para
quién, funciona Internet, se vuelve cada vez más urgente.

